package com.example.myapplication.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;


public class student_adapter extends RecyclerView.Adapter<student_adapter.StudentViewHolder> {

    private String[] student_name;
    private String[] student_contact;
    private String[] student_address;
    public student_adapter( String[] student_name, String[] student_contact, String[] student_address)
    {
        this.student_name=student_name;
        this.student_contact=student_contact;
        this.student_address=student_address;
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_students,parent,false);
        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
        holder.student_name.setText(student_name[position]);
        holder.student_contact.setText(student_contact[position]);
        holder.student_address.setText(student_address[position]);
        holder.imageView.setImageResource(R.drawable.student);
    }

    @Override
    public int getItemCount() {
        return student_name.length;
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder
    {
        TextView student_name,student_contact,student_address;
        ImageView imageView ;


        public StudentViewHolder(@NonNull View itemView) {
            super(itemView);
            student_name=itemView.findViewById(R.id.Student_name);
            student_contact=itemView.findViewById(R.id.contact);
            student_address=itemView.findViewById(R.id.address);
            imageView=itemView.findViewById(R.id.image1);
        }
    }
}
