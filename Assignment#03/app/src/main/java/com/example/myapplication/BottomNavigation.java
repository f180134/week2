package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavigation extends AppCompatActivity {

    BottomNavigationView bnv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.FrameContainer,new CourseFragment()).commit();

        bnv=(BottomNavigationView)findViewById(R.id.Bottom_nav);
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Fragment temp = null;

                switch (item.getItemId())
                {
                    case R.id.menu_course:temp=new CourseFragment();
                    break;
                    case R.id.menu_students:temp=new StudentFragment();
                    break;
                    case R.id.menu_sections:temp=new SectionFragment();

                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.FrameContainer,temp).commit();
                return true;
            }
        });

    }
}