package com.example.myapplication.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.myapplication.A_section_fragment;
import com.example.myapplication.B_section_fragment;
import com.example.myapplication.C_section_fragment;

public class page_adapter extends FragmentPagerAdapter {
    int tabcount;
    public page_adapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        tabcount=behavior;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0: return new A_section_fragment();
            case 1: return new B_section_fragment();
            case 2: return new C_section_fragment();
            default: return null;
        }

    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
