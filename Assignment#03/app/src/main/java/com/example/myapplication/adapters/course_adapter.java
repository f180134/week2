package com.example.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.CourseFragment;
import com.example.myapplication.R;

public class course_adapter extends RecyclerView.Adapter<course_adapter.CourseViewHolder> {

    private Context context;
    private String[] course_name;
    private String[] course_code;
    private String[] course_description;
    public course_adapter( String[] course_name, String[] course_code, String[] course_description)
    {
        this.course_name=course_name;
        this.course_code=course_code;
        this.course_description=course_description;
    }

    @NonNull
    @Override
    public CourseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_courses,parent,false);
        return new CourseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseViewHolder holder, int position) {
        holder.course_code.setText(course_code[position]);
        holder.course_name.setText(course_name[position]);
        holder.course_description.setText(course_description[position]);
    }

    @Override
    public int getItemCount() {
        return course_name.length;
    }

    public class CourseViewHolder extends RecyclerView.ViewHolder
    {
        TextView course_name,course_code,course_description;

        public CourseViewHolder(@NonNull View itemView) {
            super(itemView);
            course_name=itemView.findViewById(R.id.course_name);
            course_code=itemView.findViewById(R.id.course_code);
            course_description=itemView.findViewById(R.id.contact_desc);
        }
    }
}
