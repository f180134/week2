package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapters.student_adapter;

public class B_section_fragment extends Fragment {
    RecyclerView recyclerView;
    private String[] student_name={"Affan Zahid","Hassan Maqbool"};
    private String[] student_contact={"+92-736012831","+92-095012831"};
    private String[] student_Address={ "(731) 235-1445 ," + "607 124th Hwy\n" + "Greenfield, Tennessee(TN), 38230",
            "(276) 956-4578 ," + "5047 Soapstone Rd\n" + "Ridgeway, Virginia(VA), 24148"};

    public B_section_fragment() {
        // Required empty public constructor
    }
    public static B_section_fragment newInstance(String param1, String param2) {
        B_section_fragment fragment = new B_section_fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.b_section_fragment, container, false);
        recyclerView = view.findViewById(R.id.sec_B_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new student_adapter(student_name,student_contact,student_Address));
        return view;

    }
}