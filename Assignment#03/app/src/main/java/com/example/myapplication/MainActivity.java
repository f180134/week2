package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText user_mail, user_pass;
    Button btn_login;
    String mail, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user_mail = findViewById(R.id.edittext_fname);
        user_pass=findViewById(R.id.pass_input);
        btn_login=findViewById(R.id.signup_button);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        mail = user_mail.getText().toString();
        pass = user_pass.getText().toString();
        if (id == R.id.signup_button) {

            if (mail.equals("Ali") && pass.equals("12345"))
            {
              Intent intent =new Intent(this,BottomNavigation.class);
              startActivity(intent);
                user_pass.setText("");
                user_mail.setText("");
            }
            else if (mail.equals("") || pass.equals(""))
            {
                if (mail.equals(""))
                {
                    user_mail.setError("Email is required");
                }
                if (mail.equals("")) {
                    user_pass.setError("Password is required");
                }
            } else {
                //Display appropriate dialog box if id does not match with "f180134"
                AlertDialog dlg = new AlertDialog.Builder(this)
                        .setTitle("Alert")
                        .setMessage("Invalid User")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                dlg.show();


            }
        }


    }
}