package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapters.student_adapter;

public class A_section_fragment extends Fragment {

    RecyclerView recyclerView;
    private String[] student_name={"Ali Zahid","Shaheer Khan","Aamir Sohail"};
    private String[] student_contact={"+92-109576831","+92-100982831","+92-579342831"};
    private String[] student_Address={"(919) 779-4425, " + "8200 Middleton Rd\n" + "Garner, North Carolina(NC), 27529",
            "(510) 653-4406 ," + "6363 Christie Ave #APT 2606\n" + "Emeryville, California(CA), 94608",
            "(580) 832-2269 ," + "1644 Crestview Dr\n" + "Cordell, Oklahoma(OK), 73632"};


    public A_section_fragment() {
        // Required empty public constructor
    }

    public static A_section_fragment newInstance() {
        A_section_fragment fragment = new A_section_fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_a_section_fragment, container, false);
        recyclerView = view.findViewById(R.id.sec_A_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new student_adapter(student_name,student_contact,student_Address));
        return view;
    }
}