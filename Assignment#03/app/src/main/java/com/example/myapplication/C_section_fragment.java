package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapters.student_adapter;

public class C_section_fragment extends Fragment {
    RecyclerView recyclerView;
    private String[] student_name={"John Paul","Nick Fury","John Abram"};
    private String[] student_contact={"+92-67242831","+92-836022831","+92-109045631"};
    private String[] student_Address={  "(785) 827-4782 ," + "846 Birch Dr\n" + "Salina, Kansas(KS), 67401",
            "(540) 543-2071 ," + "186 Sunrise Ln\n" + "Brightwood, Virginia(VA), 22715",
            "(317) 272-6858 ," + "7982 Cobblesprings Dr\n" + "Avon, Indiana(IN), 46123"};

    public C_section_fragment() {
        // Required empty public constructor
    }

    public static C_section_fragment newInstance(String param1, String param2) {
        C_section_fragment fragment = new C_section_fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.c_section_fragment, container, false);
        recyclerView = view.findViewById(R.id.sec_C_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new student_adapter(student_name,student_contact,student_Address));
        return view;

    }
}