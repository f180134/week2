package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapters.course_adapter;

public class CourseFragment extends Fragment {


    RecyclerView recyclerView;
    private String[] course_name={"Android development","Web Programming","Programming Fudamentals"
            ,"Software Testing","Information Security","Data Structures","Computer Networks","Software Engineering"};
    private String[] course_code={"CS440","CS406","CS118"
            ,"CS4036","CS3002","CS218","CS307","CS303"};
    private String[] course_description={"Intoduction to Android development","Intoduction to Web Programming",
            "Intoduction to Programming Fudamentals","Intoduction to Software Testing",
            "Intoduction to Information Security","Intoduction to Data Structures",
            "Intoduction to Computer Networks","Intoduction to Software Engineering"};

    public CourseFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CourseFragment newInstance() {
        CourseFragment fragment = new CourseFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_course2, container, false);
        recyclerView = view.findViewById(R.id.course_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new course_adapter(course_name,course_code,course_description));
        return view;
    }
}