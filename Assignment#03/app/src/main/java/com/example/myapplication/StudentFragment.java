package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapters.course_adapter;
import com.example.myapplication.adapters.student_adapter;


public class StudentFragment extends Fragment {

    RecyclerView recyclerView;
    private String[] student_name={"Ali Zahid","Shaheer Khan","Aamir Sohail"
            ,"Affan Zahid","Hassan Maqbool","John Paul","Nick Fury","John Abram"};
    private String[] student_contact={"+92-109576831","+92-100982831","+92-579342831"
            ,"+92-736012831","+92-095012831","+92-67242831","+92-836022831","+92-109045631"};
    private String[] student_Address={"(919) 779-4425, " + "8200 Middleton Rd\n" + "Garner, North Carolina(NC), 27529",
            "(510) 653-4406 ," + "6363 Christie Ave #APT 2606\n" + "Emeryville, California(CA), 94608",
            "(580) 832-2269 ," + "1644 Crestview Dr\n" + "Cordell, Oklahoma(OK), 73632",
            "(731) 235-1445 ," + "607 124th Hwy\n" + "Greenfield, Tennessee(TN), 38230",
            "(276) 956-4578 ," + "5047 Soapstone Rd\n" + "Ridgeway, Virginia(VA), 24148",
            "(785) 827-4782 ," + "846 Birch Dr\n" + "Salina, Kansas(KS), 67401",
            "(540) 543-2071 ," + "186 Sunrise Ln\n" + "Brightwood, Virginia(VA), 22715",
            "(317) 272-6858 ," + "7982 Cobblesprings Dr\n" + "Avon, Indiana(IN), 46123"};



    public StudentFragment() {
        // Required empty public constructor
    }


    public static StudentFragment newInstance() {
        StudentFragment fragment = new StudentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_student, container, false);
        recyclerView = view.findViewById(R.id.student_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new student_adapter(student_name,student_contact,student_Address));
        return view;
    }
}