package com.example.mywebapplication.Database.Daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import com.example.mywebapplication.Database.Entities.Cities_Table;
import com.example.mywebapplication.Database.Entities.WeatherRecord_table;

import java.util.List;


@Dao
public interface City_dao {

    @Query("SELECT cityid FROM Cities_Table WHERE cityName LIKE :name ")
    List<Cities_Table> searchCity(String name);

    @Insert
    void insertCity(Cities_Table cities_table);

    @Query("DELETE FROM Cities_Table")
    void delete_all_records();

    @Query("SELECT * FROM Cities_Table")
    List<Cities_Table> getCities();


}
