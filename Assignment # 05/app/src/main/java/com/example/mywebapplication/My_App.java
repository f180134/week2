package com.example.mywebapplication;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.mywebapplication.Database.database;

public class My_App extends Application {

    private static database weatherDatabase;
    public static database get_database(Context context)
    {
        if (weatherDatabase==null)
        {
            weatherDatabase= Room.databaseBuilder(context,database.class,"weather-db").allowMainThreadQueries().build();
        }
        return weatherDatabase;
    }

}
