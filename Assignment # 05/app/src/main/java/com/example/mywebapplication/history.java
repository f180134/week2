package com.example.mywebapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mywebapplication.Adapters.weather_adapter;
import com.example.mywebapplication.Database.Entities.Cities_Table;
import com.example.mywebapplication.Database.Entities.WeatherRecord_table;
import com.example.mywebapplication.Database.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class history extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    weather_adapter Weather_adapter;
    //To hold weather records
    ArrayList<WeatherRecord_table> weather_records= new ArrayList<WeatherRecord_table>();
    //To hold city records that are searched by user
    ArrayList<Cities_Table> cities_records= new ArrayList<Cities_Table>();
    database weatherDatabase;
    ArrayList<String> city_names=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        // Getting singleton object of database
        weatherDatabase= My_App.get_database(getBaseContext());
        //Binding spinner
        Spinner city_spinner;
        city_spinner=findViewById(R.id.cityspinner);

        //Getting all cities names from city table (Cities that user searched)
        List<Cities_Table> city_list= weatherDatabase.city_dao().getCities();
        cities_records.clear();
        cities_records.addAll(city_list);
        int temp=cities_records.size();
        city_names.clear();
        city_names.add("All Cities");
        for (int i = 0; i < cities_records.size(); i++) {
            city_names.add(cities_records.get(i).cityName);
        }
        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,city_names);
        city_spinner.setAdapter(arrayAdapter);
        city_spinner.setOnItemSelectedListener(this);

        //Set adapter and recycler view
        Weather_adapter= new weather_adapter(this,weather_records);
        RecyclerView recyclerView=findViewById(R.id.weather_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(Weather_adapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String selected_name= parent.getItemAtPosition(position).toString();
        if (selected_name=="All Cities")
        {
            Toast.makeText(this, selected_name, Toast.LENGTH_SHORT).show();
            get_all_records();
        }
        else
        {
            Toast.makeText(this, selected_name, Toast.LENGTH_SHORT).show();
            get_specific_city_records(selected_name);
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void get_all_records()
    {
        List<WeatherRecord_table> weather_list= weatherDatabase.Weather_dao().getWeather();
        weather_records.clear();
        weather_records.addAll(weather_list);
        Weather_adapter.notifyDataSetChanged();

    }

    public void get_specific_city_records(String name)
    {
        List<Cities_Table> city_id=weatherDatabase.city_dao().searchCity(name);
        int id=city_id.get(0).cityid;
        List<WeatherRecord_table> weather_list= weatherDatabase.Weather_dao().getCityWeather(id);
        weather_records.clear();
        weather_records.addAll(weather_list);
        Weather_adapter.notifyDataSetChanged();
    }
}