package com.example.mywebapplication.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mywebapplication.Database.Entities.WeatherRecord_table;
import com.example.mywebapplication.R;

import java.util.ArrayList;

public class weather_adapter extends RecyclerView.Adapter<weather_adapter.WeatherViewHolder> {

    ArrayList<WeatherRecord_table> weather;
    private Context context;

    public weather_adapter( Context  context,    ArrayList<WeatherRecord_table> weathers )
    {
        this.context=context;
        this.weather=weathers;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_table,parent,false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {

        holder.city_id.setText(String.valueOf(weather.get(position).city_id));
        holder.record_id.setText(String.valueOf(weather.get(position).recordid));
        holder.date.setText(weather.get(position).record_date);
        holder.temp.setText(weather.get(position).Temperature);
        holder.desc.setText(weather.get(position).Description);
        holder.speed.setText(weather.get(position).wind_Speed);
        holder.dir.setText(weather.get(position).wind_dir);
        holder.precip.setText(weather.get(position).Precip);
        holder.hum.setText(weather.get(position).humidity);
    }
    @Override
    public int getItemCount() {
        return weather.size();
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder
    {
        TextView city_id,record_id,date,temp,desc,speed,dir,precip,hum;

        public WeatherViewHolder(@NonNull View itemView) {
            super(itemView);

            city_id=itemView.findViewById(R.id.city_id);
            record_id=itemView.findViewById(R.id.record_id);
            date=itemView.findViewById(R.id.record_date);
            temp=itemView.findViewById(R.id.temp_txt);
            desc=itemView.findViewById(R.id.desc_txt);
            speed=itemView.findViewById(R.id.wind_speed);
            dir=itemView.findViewById(R.id.wind_dir);
            precip=itemView.findViewById(R.id.precip_txt);
            hum=itemView.findViewById(R.id.hum_txt);

        }
    }
}
