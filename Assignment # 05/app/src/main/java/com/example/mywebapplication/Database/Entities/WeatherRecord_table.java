package com.example.mywebapplication.Database.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName="WeatherRecords")
public class WeatherRecord_table implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int recordid;

    @ColumnInfo(name="cityid")
    public int city_id;

    @ColumnInfo(name="recordDate")
    public String record_date;

    @ColumnInfo(name="temperature")
    public String Temperature;

    @ColumnInfo(name="description")
    public String Description;

    @ColumnInfo(name="windspeed")
    public String wind_Speed;

    @ColumnInfo(name="windDir")
    public String wind_dir;

    @ColumnInfo(name="precip")
    public String Precip;

    @ColumnInfo(name="Humidity")
    public String humidity;
}


