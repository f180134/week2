package com.example.mywebapplication.Database.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mywebapplication.Database.Entities.Cities_Table;
import com.example.mywebapplication.Database.Entities.WeatherRecord_table;

import java.util.List;


@Dao
public interface weather_dao {


    @Query("SELECT * FROM WeatherRecords")
    List<WeatherRecord_table> getWeather();

    @Insert
    void insertWeather(WeatherRecord_table weatherRecords);

    @Query("SELECT * FROM WeatherRecords where cityid LIKE :city_Id ")
    List<WeatherRecord_table> getCityWeather(int city_Id);


    @Query("DELETE FROM WeatherRecords")
    void delete_all_records();
}
