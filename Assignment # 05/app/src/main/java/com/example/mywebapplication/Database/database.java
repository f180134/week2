package com.example.mywebapplication.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.mywebapplication.Database.Daos.City_dao;
import com.example.mywebapplication.Database.Daos.weather_dao;
import com.example.mywebapplication.Database.Entities.Cities_Table;
import com.example.mywebapplication.Database.Entities.WeatherRecord_table;

@Database(entities = {WeatherRecord_table.class, Cities_Table.class},version = 1)
public abstract class database extends RoomDatabase{

    public  abstract City_dao city_dao();
    public abstract weather_dao Weather_dao();
}

