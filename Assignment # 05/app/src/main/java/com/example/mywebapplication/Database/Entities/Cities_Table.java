package com.example.mywebapplication.Database.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName="Cities_Table")
public class Cities_Table {

    @PrimaryKey(autoGenerate = true)
    public int cityid;

    @ColumnInfo(name="cityName")
    public String cityName;

}
