package com.example.mywebapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mywebapplication.Adapters.weather_adapter;
import com.example.mywebapplication.Database.Entities.Cities_Table;
import com.example.mywebapplication.Database.Entities.WeatherRecord_table;
import com.example.mywebapplication.Database.database;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.io.Serializable;
import java.util.List;

public class WeatherActivity extends AppCompatActivity {
    TextView City_name,Count_name,Desc,Hum,Speed,Pres,Dir,Temp,date,Cloud,Visible,Feel,Pressure;
    String city_name,count_name,desc,hum,speed,pres,dir,temp,w_icon,cloud,visible,feel,pressure;
    ImageView imageView;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat record_dateFormat;
    private String date_txt;
    private String record_date_txt;
    ImageView history_btn;
    database weatherDatabase;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        //Create Sigleton object of Database
        weatherDatabase= My_App.get_database(getBaseContext());



        //Binding Xml components with java file
        City_name=findViewById(R.id.city_txt);
        Count_name=findViewById(R.id.country_txt);
        Desc=findViewById(R.id.desc_txt);
        Hum=findViewById(R.id.hum_txt);
        Speed=findViewById(R.id.wspeed_txt);
        Pres=findViewById(R.id.precip_txt);
        Dir=findViewById(R.id.wdir_txt);
        Temp=findViewById(R.id.temp_txt);
        Cloud=findViewById(R.id.cloud_cover);
        Visible=findViewById(R.id.visibility);
        Feel=findViewById(R.id.feellike);
        Pressure=findViewById(R.id.pressure);
        date=findViewById(R.id.Date_id);
        imageView=findViewById(R.id.imageView);
        history_btn=findViewById(R.id.imagehistory);



        history_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent= new Intent(getApplicationContext(),history.class);
               startActivity(intent);
            }
        });

        //get instance of calender to display current date and time
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy\n\t\t\thh:mm aaa"  );
        date_txt = dateFormat.format(calendar.getTime());
        record_dateFormat= new SimpleDateFormat("dd/MM/yyyy");
        record_date_txt= record_dateFormat.format(calendar.getTime());
        date.setText(date_txt);

        //Receiving Intent sent from previous activity with city name
        Intent intent = getIntent();
        city_name= intent.getStringExtra("City_Name");
        getWeather(city_name);
    }




    //Function to get weather object from Api, parse it and then display it on the UI
    private void getWeather(String cityname)
    {
        //Making the listener ready: (Base URL,Api Call, ACCESS TOKEN)
        String URL="http://api.weatherstack.com/current?access_key=301ac0fee675869dae624567c14a58de";
        String listener=URL+"&query="+cityname;

        //Jason Request Oject, Method, listen
        JsonObjectRequest request=new JsonObjectRequest(Request.Method.GET, listener, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    //Geting object from Api response and parsing it
                    JSONObject obj = response.getJSONObject("location");
                    city_name= obj.getString("name");
                    count_name=obj.getString("country");
                    obj = response.getJSONObject("current");
                    temp=obj.getString("temperature");
                    temp=temp+"\u2103";

                    JSONArray icons=obj.getJSONArray("weather_icons");
                    w_icon= icons.getString(0);

                    JSONArray w_desc=obj.getJSONArray("weather_descriptions");
                    for (int i = 0; i < w_desc.length(); i++) {
                        if (i==0)
                        {
                            desc=w_desc.getString(i);
                        }
                        else
                        {
                            desc=desc+w_desc.getString(i);
                        }
                        if(i!=w_desc.length()-1)
                        {
                            desc=desc+" , ";
                        }
                    }
                    speed=obj.getString("wind_speed");
                    speed=speed+" KM/H";
                    dir=obj.getString("wind_dir");
                    pres=obj.getString("precip");
                    pres=pres+" mm";
                    hum=obj.getString("humidity");
                    hum=hum+" %";

                    cloud=obj.getString("cloudcover");
                    cloud=cloud+" %";
                    visible=obj.getString("visibility");
                    visible=visible+" Km";
                    feel=obj.getString("feelslike");
                    feel=feel+"\u2103";
                    pressure=obj.getString("pressure");
                    pressure=pressure+" MB";

                    //Get image path from api and assign to image view using picasso library
                    Picasso.with(WeatherActivity.this.getApplicationContext()).load(w_icon).into(imageView);

                    City_name.setText(city_name);
                    Count_name.setText(count_name);
                    Desc.setText(desc);
                    Speed.setText(speed);
                    Hum.setText(hum);
                    Pres.setText(pres);
                    Dir.setText(dir);
                    Temp.setText(temp);
                    Cloud.setText(cloud);
                    Visible.setText(visible);
                    Feel.setText(feel);
                    Pressure.setText(pressure);

                    //Search if city is already searched or not
                    List<Cities_Table> city_list=weatherDatabase.city_dao().searchCity(city_name);
                    if(city_list.isEmpty())
                    {
                        Cities_Table cities=new Cities_Table();
                        cities.cityName=city_name;
                        weatherDatabase.city_dao().insertCity(cities);
                        Toast.makeText(WeatherActivity.this, "City inserted: "+city_name, Toast.LENGTH_SHORT).show();

                    }
                    List<Cities_Table> temp_list=weatherDatabase.city_dao().searchCity(city_name);
                    WeatherRecord_table history=new WeatherRecord_table();
                    history.city_id= temp_list.get(0).cityid;
                    history.record_date=record_date_txt;
                    history.Description=desc;
                    history.humidity=hum;
                    history.wind_Speed=speed;
                    history.Precip=pres;
                    history.wind_dir=dir;
                    history.Temperature=temp;
                    weatherDatabase.Weather_dao().insertWeather(history);
                    Toast.makeText(WeatherActivity.this, "Weather History Inserted: "+city_name, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(WeatherActivity.this, (CharSequence) e, Toast.LENGTH_SHORT).show();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WeatherActivity.this, "Response error from Api", Toast.LENGTH_SHORT).show();
            }
        });

        //Volley Request queue for making api requests
        Volley.newRequestQueue(this).add(request);
    }

    private void delete_all_records()
    {
        weatherDatabase.Weather_dao().delete_all_records();
        weatherDatabase.city_dao().delete_all_records();
    }

}