package com.example.mywebapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText1;
    Button btn1;
    String city_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.City_name);
        btn1 = findViewById(R.id.weather_btn);
        btn1.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        city_name = editText1.getText().toString();
        if (id == R.id.weather_btn) {
            if (city_name.equals(""))
            {
                editText1.setError("This field cannot be empty!");
            }
            else
            {
                Intent intent =new Intent(this,WeatherActivity.class);
                intent.putExtra("City_Name",city_name);
                editText1.setText("");
                startActivity(intent);
            }
        }
    }
}