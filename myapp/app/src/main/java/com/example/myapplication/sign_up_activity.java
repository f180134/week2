package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class sign_up_activity extends AppCompatActivity implements View.OnClickListener {

    Button btn_signup;
    EditText Fname, Lname, Email, pass, c_pass, phone, address, roll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        btn_signup = findViewById(R.id.signup_button);
        btn_signup.setOnClickListener(this);
        Fname = findViewById(R.id.edittext_fname);
        Lname = findViewById(R.id.edittext_lname);
        Email = findViewById(R.id.edittext_mail);
        pass = findViewById(R.id.pass_input);
        c_pass = findViewById(R.id.confirmpass_input);
        address = findViewById(R.id.address_input);
        phone = findViewById(R.id.phone_input);
        roll = findViewById(R.id.roll_input);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        String check_fname, check_lname, check_Email, check_pass, check_c_pass, check_phone, check_address, check_roll;
        check_fname = Fname.getText().toString();
        check_lname = Fname.getText().toString();
        check_Email = Fname.getText().toString();
        check_pass = Fname.getText().toString();
        check_c_pass = Fname.getText().toString();
        check_phone = Fname.getText().toString();
        check_address = address.getText().toString();
        check_roll = roll.getText().toString();


        if (id == R.id.signup_button) {
            if (check_fname.equals("") || check_lname.equals("") || check_Email.equals("") || check_pass.equals("") || check_c_pass.equals("") || check_phone.equals("") || check_roll.equals("")) {
                if (check_fname.equals("")) {
                    Fname.setError("First Name is required");
                }
                if (check_lname.equals("")) {
                    Lname.setError("Last Name is required");
                }
                if (check_Email.equals("")) {
                    Email.setError("Email is required");
                }
                if (check_pass.equals("")) {
                    pass.setError("Password is required");
                }
                if (check_c_pass.equals("")) {
                    c_pass.setError("Repeat Password is required");
                }
                if (check_phone.equals("")) {
                    phone.setError("Phone Number is required");
                }
                if (check_roll.equals("")) {
                    roll.setError("Roll No. is required");
                }
            } else {

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);


            }

        }

    }
}