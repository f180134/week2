package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//PROJECT CREATION
//Roll No: F180134
//1) Empty activity was selected as an activity template for project creation.
//2) Api level 30 was selected.
//3) Android version 11.0 lollipop was selected since it gave authority to 94.3% android mobiles to run this app
//4) Pixel API 30 with android version 11.0 is created as an android virtual machine.

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText user_mail, user_pass;
    Button btn_login;
    String mail, pass;
    TextView btn_sign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user_mail = findViewById(R.id.edittext_fname);
        user_pass = findViewById(R.id.pass_input);
        btn_login = findViewById(R.id.signup_button);
        btn_login.setOnClickListener(this);
        btn_sign = findViewById(R.id.sign_up_label);
        btn_sign.setOnClickListener(this);

    }

    //This function will call whenever user will click on button
    @Override
    public void onClick(View view) {
        int id = view.getId();
        mail = user_mail.getText().toString();
        pass = user_pass.getText().toString();
        if (id == R.id.signup_button) {

            if (mail.equals("f180134@nu.edu.pk") && pass.equals("123456789")) {
                Intent intent =new Intent(this,home_screen_activity.class);

                startActivity(intent);
                user_pass.setText("");
                user_mail.setText("");


            }
            else if (mail.equals("") || pass.equals(""))
            {
                if (mail.equals(""))
                {
                    user_mail.setError("Email is required");
                }
                if (mail.equals(""))
                {
                    user_pass.setError("Password is required");
                }
            }
            else {
                //Display appropriate dialog box if id does not match with "f180134"
                AlertDialog dlg = new AlertDialog.Builder(this)
                        .setTitle("Alert")
                        .setMessage("Invalid User")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create();
                dlg.show();


            }


        }
        else if(id == R.id.sign_up_label)
        {
            Intent intent =new Intent(this,sign_up_activity.class);
            startActivity(intent);
        }

    }
}