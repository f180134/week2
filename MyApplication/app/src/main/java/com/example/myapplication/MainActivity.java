package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

       //PROJECT CREATION
//Roll No: F180134
//1) Empty activity was selected as an activity template for project creation.
//2) Api level 30 was selected.
//3) Android version 11.0 lollipop was selected since it gave authority to 94.3% android mobiles to run this app
//4) Pixel API 30 with android version 11.0 is created as an android virtual machine.



//Main activity class, implements onclicklisterner get access to onclicklisteners
public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText user_name;
    Button btn1;
    String S;


//This oncreate is called whenever the activity is created for the first time.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user_name=findViewById(R.id.editTextTextPersonName4);
        btn1=findViewById(R.id.button);
        btn1.setOnClickListener(this);


    }
//This function will call whenever user will click on a button
    @Override
    public void onClick(View view) {
        int id=view.getId();
        S=user_name.getText().toString();
        if (id==R.id.button) {
            if (S.equals("f180134"))
            {
                //Display toast if user ID matches with "f180134"
                Toast.makeText(this, "Welcome "+S, Toast.LENGTH_SHORT).show();
            }
            else
            {
                //Display appropriate dialog box if id does not match with "f180134"
               AlertDialog dlg= new AlertDialog.Builder(this)
                       .setTitle("Alert")
                       .setMessage("Invalid User")
                       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               dialogInterface.dismiss();
                           }
                       })
                       .create();
                       dlg.show();


            }


        }


    }
}